<?php

use Illuminate\Database\Seeder;

class StatusesAnimalsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('status_animals')->insert([
            [
                'description' => 'Perdido',
            ],
            [
                'description' => 'Comunicado',
            ],
            [
                'description' => 'Encontrado',
            ]
        ]);
    }
}
