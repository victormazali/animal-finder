<?php

use App\User;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index');
Route::post('animals/foundAnimal', 'AnimalController@foundTheAnimal')->name('animals.foundAnimal');
Route::get('/register', 'UserController@register')->name('register');
Route::post('register/store', 'UserController@storeRegister')->name('storeRegister');

Route::group(['middleware' => 'auth'], function () {
	
	Route::resource('users', 'UserController');

    Route::resource('animals', 'AnimalController');
    Route::get('animals/showFinders/{animal}', 'AnimalController@showFinders')->name('animals.showFinders');
    
});