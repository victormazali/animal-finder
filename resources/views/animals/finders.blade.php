@extends('layouts.default')

@section('title')
  Meus Animais
@endsection

@section('breadcumb')
	<li>
		Animais
	</li>
@endsection

@section('content')
	
    <h1 class="page-title">
    	Animais
        <small>Index</small>
    </h1>

    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase">Comunicados do Animal</b></span>
                    </div>
                    <div class="tools">

                    </div>
                </div>
                <div class="portlet-body table-both-scroll">
                    <table class="table table-striped table-bordered table-hover order-column" id="">
                        <thead>
                            <tr>
                                <th>Foto</th>
                                <th>Nome</th>
                                <th>Telefone</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($animal->finders as $r)
                            <tr>
                                <td>
                                    <img class="img-responsive" style="width: 100px; height: 100px;" src="{{asset($animal->picture)}}">
                                </td>
                                <td>{{$r->name}}</td>
                                <td>{{$r->telefone}}</td>
                            </tr>
                        @endforeach    
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
    
@endsection

@section('before-plugins')
        <!--Datatable-->
        <script src="{{ asset("assets/global/scripts/datatable.js") }}" type="text/javascript"></script>
        <script src="{{ asset("assets/global/plugins/datatables/datatables.min.js") }}" type="text/javascript"></script>
        <script src="{{ asset("assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js") }}" type="text/javascript"></script>
        <script src="{{ asset("assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js") }}" type="text/javascript"></script>

@endsection

@section('after-plugins')
        <!--Datatable-->
        <script src="{{ asset("assets/pages/scripts/table-datatables-buttons.min.js") }}" type="text/javascript"></script>


@endsection

@section('scripts')
 
@endsection