<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.7
Version: 4.7.1
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>@yield('title')</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="ONG Animal Finder" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="{{ asset("assets/global/plugins/font-awesome/css/font-awesome.min.css") }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset("assets/global/plugins/simple-line-icons/simple-line-icons.min.css") }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset("assets/global/plugins/bootstrap/css/bootstrap.min.css") }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset("assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css") }}" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="{{ asset("assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css") }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset("assets/global/plugins/morris/morris.css") }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset("assets/global/plugins/jqvmap/jqvmap/jqvmap.css") }}" rel="stylesheet" type="text/css" />


        <!--Date-->
        <link href="{{ asset("assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css") }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset("assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css") }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset("assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css") }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset("assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset("assets/global/plugins/clockface/css/clockface.css") }}" rel="stylesheet" type="text/css" />

        <!--Select -->
        <link href="{{ asset("assets/global/plugins/select2/css/select2.min.css") }}" rel="stylesheet" type="text/css" /> <!-- esse aqui altera o placeholder do multiselect -->
        <link href="{{ asset("assets/global/plugins/select2/css/select2-bootstrap.min.css") }}" rel="stylesheet" type="text/css" />
        
        @yield('css-plugins')
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="{{ asset("assets/global/css/components-md.min.css") }}" rel="stylesheet" id="style_components" type="text/css" />
        <link href="{{ asset("assets/global/css/plugins-md.min.css") }}" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="{{ asset("assets/layouts/layout/css/layout.min.css") }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset("assets/layouts/layout/css/themes/default.min.css") }}" rel="stylesheet" type="text/css" id="style_color" />
        <link href="{{ asset("assets/layouts/layout/css/custom.min.css") }}" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        <link rel="icon" href="#" type="image/x-icon" />
        <link rel="shortcut icon" href="#" type="image/x-icon" />
        </head>
    <!-- END HEAD -->

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white page-md">
        <div class="page-wrapper">
            <!-- BEGIN HEADER -->
            @include('partials.header')
            <!-- END HEADER -->
            <!-- BEGIN HEADER & CONTENT DIVIDER -->
            <div class="clearfix"> </div>
            <!-- END HEADER & CONTENT DIVIDER -->
            <!-- BEGIN CONTAINER -->
            <div class="page-container">
                <!-- BEGIN SIDEBAR -->
                <div class="page-sidebar-wrapper">
                    <!-- BEGIN SIDEBAR -->
                    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                    <div class="page-sidebar navbar-collapse collapse">
                        <!-- BEGIN SIDEBAR MENU -->
                        <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
                        <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
                        <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
                        <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                        <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
                        <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                        @include('partials.menu')
                        <!-- END SIDEBAR MENU -->
                        <!-- END SIDEBAR MENU -->
                    </div>
                    <!-- END SIDEBAR -->
                </div>
                <!-- END SIDEBAR -->
                <!-- BEGIN CONTENT -->
                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                    	<div class="page-bar">
                            <ul class="page-breadcrumb">
                            	@yield('breadcumb')
                            </ul>
                             <div class="page-toolbar">
                                @yield('helper')
                             </div>
                        </div>
                         @if(session()->has('flash_message'))
                            <div class="alert alert-{{ session('flash_type')}}" style="margin-top:5px;">
                                {{ session('flash_message') }}
                            </div>
                        @endif
                        <div id="flash-msg">
                            @include('flash::message')
                        </div>
                        @yield('content')
                    </div>
                    <!-- END CONTENT BODY -->
                </div>
                <!-- END CONTENT -->
            </div>
            <!-- END CONTAINER -->
            <!-- BEGIN FOOTER -->
            
            <!-- END FOOTER -->
        </div>
        <!--[if lt IE 9]>
<script src="../assets/global/plugins/respond.min.js"></script>
<script src="../assets/global/plugins/excanvas.min.js"></script> 
<script src="../assets/global/plugins/ie8.fix.min.js"></script> 
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="{{ asset("assets/global/plugins/jquery.min.js") }}") }}" type="text/javascript"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.js" type="text/javascript"></script>
        <script src="{{ asset("assets/global/plugins/bootstrap/js/bootstrap.min.js") }}" type="text/javascript"></script>
        <script src="{{ asset("assets/global/plugins/js.cookie.min.js") }}" type="text/javascript"></script>
        <script src="{{ asset("assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js") }}" type="text/javascript"></script>
        <script src="{{ asset("assets/global/plugins/jquery.blockui.min.js") }}" type="text/javascript"></script>
        <script src="{{ asset("assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js") }}" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->

        <!--Date-->
        <script src="{{ asset("assets/global/plugins/moment.min.js") }}" type="text/javascript"></script>
        <script src="{{ asset("assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js") }}" type="text/javascript"></script>
        <script src="{{ asset("assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js") }}" type="text/javascript"></script>
        <script src="{{ asset("assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js") }}" type="text/javascript"></script>
        <script src="{{ asset("assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js") }}" type="text/javascript"></script>
        <script src="{{ asset("assets/global/plugins/clockface/js/clockface.js") }}" type="text/javascript"></script>

        <!--Select-->
        <script src="{{ asset("assets/global/plugins/select2/js/select2.full.min.js") }}" type="text/javascript"></script>
        @yield('before-plugins')
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        
        <script src="{{ asset("js/custom_masks.js") }}"></script>
        <script src="{{ asset("assets/global/scripts/app.min.js") }}" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <!--Date-->
        <script src="{{ asset("assets/pages/scripts/components-date-time-pickers.min.js") }}" type="text/javascript"></script>
        <!--Select-->
        <script src="{{ asset("assets/pages/scripts/components-select2.min.js") }}" type="text/javascript"></script>

        <!--Block UI-->
        <script src="{{ asset("assets/pages/scripts/ui-blockui.js") }}" type="text/javascript"></script>
        @yield('after-plugins')
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="{{ asset("assets/layouts/layout/scripts/layout.min.js") }}" type="text/javascript"></script>
        <script src="{{ asset("assets/layouts/layout/scripts/demo.min.js") }}" type="text/javascript"></script>
        <script src="{{ asset("assets/layouts/global/scripts/quick-sidebar.min.js") }}" type="text/javascript"></script>
        <script src="{{ asset("assets/layouts/global/scripts/quick-nav.min.js") }}" type="text/javascript"></script>
        <!-- END THEME LAYOUT SCRIPTS -->
        @yield('script')
        <script type="text/javascript">
            $(function () {
                // flash auto hide
                $('#flash-msg .alert').not('.alert-danger, .alert-important').delay(6000).slideUp(500);
            })
        </script>
    </body>

</html>