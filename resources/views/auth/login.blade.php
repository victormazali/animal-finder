@extends('layouts.auth')

@section('content')
<!-- BEGIN LOGO -->
        <div class="logo">
            <a href="#">
                <img src="" alt="" width="340" /> </a>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN LOGIN -->
        <div class="content">
            <!-- BEGIN LOGIN FORM -->
            <form class="login-form" role="form" method="POST" action="{{ url('/login') }}">
            {{ csrf_field() }}
                <h3 class="form-title" style="color: #ccc">Entrar</h3>
                @if(session()->has('flash_message'))
                    <div class="alert alert-{{ session('flash_type')}}" style="margin-top:5px;">
                        {{ session('flash_message') }}
                    </div>
                @endif
                <div class="alert alert-danger display-hide">
                    <button class="close" data-close="alert"></button>
                    <span> Enter any username and password. </span>
                </div>
                <div class="form-group">
                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                    <label class="control-label visible-ie8 visible-ie9">Username</label>
                    <div class="input-icon">
                    <i class="fa fa-user"></i>
                    <input id="email" class="form-control form-control-solid placeholder-no-fix" type="text" placeholder="E-mail" name="email"/> </div></div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Password</label>
                    <div class="input-icon">
                        <i class="fa fa-lock"></i>
                    <input id="password" class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password" /> </div></div>
                <div class="form-actions">
                    <button type="submit" class="btn uppercase" style="background: #C83639; color: #ffffff">Login</button>

                    <a href="{{route('register')}}" class="btn green"> Cadastrar-se </a>
                    
                </div>
                
            </form>
            <!-- END LOGIN FORM -->
            <!-- BEGIN FORGOT PASSWORD FORM -->
        </div>
        <div class="copyright"> 2020 © Animal Finder - Tray. </div>
@endsection
