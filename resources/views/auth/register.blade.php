@extends('layouts.auth')

@section('content')
<!-- BEGIN LOGO -->
        <!-- END LOGO -->
        <!-- BEGIN LOGIN -->
        <div class="content">
            <!-- BEGIN LOGIN FORM -->
            <form class="login-form" role="form" method="POST" action="{{ route('storeRegister')}}">
            {{ csrf_field() }}
            <h3 class="form-title" style="color: #ccc">Cadastrar</h3>
                <div class="form-group">
                    <label>Nome</label>
                        <input placeholder="Seu nome" name="name" type="text" class="form-control">
                </div>
                <div class="form-group">
                    <label>Email</label>
                        <input placeholder="Seu email" name="email" type="email" class="form-control">
                </div>
                <div class="form-group">
                    <label>Telefone</label>
                        <input placeholder="Seu telefone" name="telefone" type="text" class="form-control">
                </div>
                <div class="form-group">
                    <label>Senha</label>
                        <input placeholder="Password" name="password" type="text" class="form-control">
                </div>
                   
                <div class="form-actions">
                    <button type="submit" class="btn uppercase" style="background: #C83639; color: #ffffff">Cadastrar</button>
                    
                </div>
                
            </form>
            <!-- END LOGIN FORM -->
            <!-- BEGIN FORGOT PASSWORD FORM -->
        </div>
        <div class="copyright"> 2020 © Animal Finder - Tray. </div>
@endsection
