@extends('layouts.default')

@section('title')
  Home
@endsection

@section('breadcumb')
	<li>
		Home
	</li>
@endsection


@section('content')


    <h1 class="page-title">
    	Home
        <small>Animais cadastrados</small>
    </h1>

	
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase">Animais Cadastrados</b></span>
                    </div>
                    <div class="tools">

                    </div>
                </div>
                <div class="portlet-body table-both-scroll">
                    <table class="table table-striped table-bordered table-hover order-column" id="">
                        <thead>
                            <tr>
                                <th>Foto</th>
                                <th>Nome</th>
                                <th>Idade</th>
                                <th>Inf.</th>
                                <th>Cidade</th>
                                <th>Estado</th>
                                <th>Ações</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($result as $r)
                            <tr>
                                <td>
                                    <img class="img-responsive" style="width: 100px; height: 100px;" src="{{asset($r->picture)}}">
                                </td>
                                <td>{{$r->name}}</td>
                                <td>{{$r->age}}</td>
                                <td>{{$r->note}}</td>
                                <td>{{$r->city}}</td>
                                <td>{{$r->state}}</td>
                                <td>
                                    <a class="btn-open-modal" href="#foundAnimal" data-toggle="modal" data-animal_id="{{$r->id}}">
                                        <button class="btn btn-xs red" type="button" >
                                            <i class="fa fa-search"></i> Encontrei!
                                        </button>
                                    </a>
                                </td>
                            </tr>
                        @endforeach    
                        </tbody>
                    </table>
                    {{ $result->appends($_GET)->links() }}
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>

<div id="foundAnimal" class="modal fade" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"> Comunicar </h4>
            </div>
            <form role="form" action="{{route('animals.foundAnimal')}}" id="formContact" METHOD="POST">
            <input type="hidden" name="animal_id" id="modal-animal_id">
            <div class="modal-body">
                {{ csrf_field() }}
                <div class="form-body">
                    <div class="form-group">
                        <label>Nome</label>
                            <input placeholder="Seu nome" name="name" type="text" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Celular</label>
                            <input placeholder="O número do seu celular" name="telefone" type="text" class="form-control" class="cellphone-mask">
                    </div>
                </div> 
                <div id="form-errors"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn dark btn-outline" data-dismiss="modal">Cancelar</button>
                <button id="modal-submit" type="submit" class="btn green">
                    <i class="fa fa-paw"></i> Registrar</button>
            </div>
            </form>
        </div>
    </div>
</div>

@endsection


@section('script')
<script type="text/javascript">
    $(document).ready(function() {
        $('#foundAnimal').on('hidden.bs.modal', function () {
            $(this).find('#formContact').trigger('reset');
            $('#form-errors').hide();
        })

        $('.btn-open-modal').on('click', function(){
            $('#modal-animal_id').val($(this).data('animal_id'));
        });

    });
</script>
@endsection