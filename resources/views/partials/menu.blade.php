<ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
    <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
    <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
    <li class="sidebar-toggler-wrapper hide">
        <div class="sidebar-toggler">
            <span></span>
        </div>
    </li>

    <!-- END SIDEBAR TOGGLER BUTTON -->
    <li class="nav-item start active">

    <li class="heading">
        <h3 class="uppercase">Home</h3>
    </li>
        <li class="nav-item {{request()->routeIs('home') ? 'active open' : ''}}">
            <a href="{{route('home')}}" class="nav-link nav-toggle">
                <i class="fa fa-home"></i>
                <span class="title">Home</span>
                <span class="{{request()->routeIs('home') ? 'selected' : ''}}"></span>
            </a>
        </li>
    <li class="heading">
        <h3 class="uppercase">Administrar</h3>
    </li>
        <li class="nav-item {{request()->routeIs('animals.index') ? 'active open' : ''}}">
            <a href="{{route('animals.index')}}" class="nav-link nav-toggle">
                <i class="fa fa-paw"></i>
                <span class="title">Meus Animais</span>
                <span class="{{request()->routeIs('animals.index') ? 'selected' : ''}}"></span>
            </a>
        </li>
</ul>