document.addEventListener('DOMContentLoaded', function() {
    var calendarEl = document.getElementById('calendar');
    
    var calendar = new FullCalendar.Calendar(calendarEl, {
      plugins: [ 'interaction', 'timeGrid' ],
      allDaySlot: false,
      slotEventOverlap: false,
      timeZone: 'UTC',
      editable: true,
      droppable: true, // this allows things to be dropped onto the calendar !!!
      // limita horario e dias que ficaram visíveis 
      header: {
        left: 'title',
        center: '',
        right: 'today, timeGridMonth, timeGridWeek, timeGridDay, prev, next'
      },
      minTime: '08:00',
      maxTime: '18:00',
      hiddenDays: [ 0 ],
      locale: 'pt-br',
      events : "http://localhost:8000/schedule/getEvents/" + "8",
      extraParams: function() {
        return {
          cachebuster: new Date().valueOf()
        };
      }
    });

  calendar.render();
});