$(document).ready(function(){
     $('.cellphone-mask').mask('(00) 00000-0000');
     $('.phone-mask').mask('(00) 0000-0000');
     $('.date-mask').mask('00/00/0000');
     $('.cpf-mask').mask('000.000.000-00');
     $('.cnpj-mask').mask('00.000.000/0000-00');
     $('.cep-mask').mask('00000-000');
     $('.time-mask').mask('00:00');
});