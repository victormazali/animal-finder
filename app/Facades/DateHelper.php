<?php

namespace App\Facades;

use App\Helpers\DateHelper as DateHelperClass;
use Illuminate\Support\Facades\Facade;

class DateHelper extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return DateHelperClass::class;
    }
}
