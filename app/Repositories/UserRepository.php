<?php

namespace App\Repositories;

use App\User;
use App\Repositories\BaseRepository;
use Carbon\Carbon;

class UserRepository extends BaseRepository
{
    /**
     * Método construtor
     *
     * @param \App\User  $model  O modelo
     */
    public function __construct(User $model)
    {
        parent::__construct($model);
    }
}
