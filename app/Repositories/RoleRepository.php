<?php

namespace App\Repositories;

use App\Role;
use App\Repositories\BaseRepository;
use Carbon\Carbon;

class RoleRepository extends BaseRepository
{
    /**
     * Método construtor
     *
     * @param \App\Role  $model  O modelo
     */
    public function __construct(Role $model)
    {
        parent::__construct($model);
    }
}
