<?php

namespace App\Repositories;

use App\Animal;
use App\Repositories\BaseRepository;
use Carbon\Carbon;

class AnimalRepository extends BaseRepository
{
    /**
     * Método construtor
     *
     * @param \App\Animal  $model  O modelo
     */
    public function __construct(Animal $model)
    {
        parent::__construct($model);
    }

    /**
     * Finds an animals for home.
     *
     * @return     \Illuminate\Support\Collection
     */
    public function findAnimalsForHome()
    {
        return $this->model->whereNotIn('status_id', [3])->paginate(10);
    }
}
