<?php

namespace App\Helpers;

use Carbon\Carbon;

class DateHelper
{
    /**
     * Método transforma uma data brasileira em objeto Carbon
     *
     * @param  string  $date  A data
     *
     * @return \Carbon\Carbon
     */
    public function brazilianDateToCarbon(string $date)
    {
        return Carbon::createFromFormat('d/m/Y', $date);
    }

    /**
     * Método transforma uma data com hora brasileira em objeto Carbon
     *
     * @param  string  $date  A data
     *
     * @return \Carbon\Carbon
     */
    public function brazilianDateTimeToCarbon(string $date)
    {
        return Carbon::createFromFormat('d/m/Y H:i', $date);
    }
}
