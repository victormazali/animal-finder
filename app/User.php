<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable, HasRoles, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 
        'email', 
        'telefone',
        'password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Get the animals for the user.
     */
    public function animals()
    {
        return $this->hasMany('App\Animal');
    }

    /**
     * Get the role that owns the user.
     */
    public function roles()
    {
        return $this->belongsToMany('App\Role', 'model_has_roles', 'model_id', 'role_id');
    }
}
