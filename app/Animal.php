<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Animal extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 
        'picture', 
        'age',
        'note',
        'city',
        'state',
        'status_id',
        'user_id'
    ];

    /**
     * Get the status that owns the animal.
     */
    public function status()
    {
        return $this->belongsTo('App\StatusAnimal');
    }

    /**
     * Get the user that owns the animal.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Get the finders for the animal.
     */
    public function finders()
    {
        return $this->hasMany('App\Finder');
    }

}
