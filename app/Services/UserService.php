<?php

namespace App\Services;

use App\Facades\DateHelper;
use App\Repositories\RoleRepository;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class UserService
{
    /**
     * Repositório de usuários
     * @var \App\Repositories\UserRepository
     */
    protected $userRepository;
    protected $roleRepository;

    /**
     * Método construtor
     *
     * @param \App\Repositories\UserRepository  $userRepository  O repositório de usuários
     */
    public function __construct(
        UserRepository $userRepository,
        RoleRepository $roleRepository
    ) {
        $this->userRepository = $userRepository;
        $this->roleRepository = $roleRepository;
    }

    /**
     * Preenche a requisição
     *
     * @param      \Illuminate\Http\Request  $request  The request
     *
     * @return     <type>                    ( description_of_the_return_value )
     */
    public function fillRequestUser(Request $request)
    {
        // hash password 
        if ($request->has('password')) {
            $request->merge(['password' => bcrypt($request->get('password'))]);
        }

        return $request;
    }

    /**
     * Stores an user.
     *
     * @param      \Illuminate\Http\Request  $request  The request
     *
     * @return     <type>                    ( description_of_the_return_value )
     */
    public function storeUser(Request $request)
    {
        return $this->userRepository->store($request->all());
    }

    /**
     * { function_description }
     *
     * @param      \Illuminate\Http\Request  $request  The request
     * @param      <type>                    $id       The identifier
     *
     * @return     <type>                    ( description_of_the_return_value )
     */
    public function updateUser(Request $request, $id)
    {
        return $this->userRepository->update($request->all(), $id);
    }

    /**
     * { function_description }
     *
     * @return     <type>  ( description_of_the_return_value )
     */
    public function findAllUsers()
    {
        return $this->userRepository->findAll();
    }

    /**
     * { function_description }
     *
     * @return     <type>  ( description_of_the_return_value )
     */
    public function findAllRoles()
    {
        return $this->roleRepository->findAll();
    }
}