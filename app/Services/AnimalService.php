<?php

namespace App\Services;

use App\Facades\DateHelper;
use App\Repositories\AnimalRepository;
use App\Repositories\FinderRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class AnimalService
{
    /**
     * Repositório de animais
     * @var \App\Repositories\animalRepository
     */
    protected $animalRepository;
    protected $finderRepository;

    /**
     * Método construtor
     *
     * @param      \App\Repositories\AnimalRepository  $animalRepository  O repositório de animais
     * @param      FinderRepository                    $finderRepository  The finder repository
     */
    public function __construct(
        AnimalRepository $animalRepository,
        FinderRepository $finderRepository
    ) {
        $this->animalRepository = $animalRepository;
        $this->finderRepository = $finderRepository;
    }
    
    /**
     * Finds an animals by user.
     *
     * @return     <type>  ( description_of_the_return_value )
     */
    public function findAnimalsByUser()
    {
        return Auth::user()->animals()->paginate(10);
    }

    /**
     * Stores an animal.
     *
     * @param      \Illuminate\Http\Request  $request  The request
     *
     * @return     <type>                    ( description_of_the_return_value )
     */
    public function storeAnimal(Request $request)
    {
        return $this->animalRepository->store($request->all());
    }

    /**
     * Preenche a requisição
     *
     * @param      \Illuminate\Http\Request  $request  The request
     *
     * @return     <type>                    ( description_of_the_return_value )
     */
    public function fillRequestAnimal(Request $request)
    {
        $file = $request->file('attachmentTemp');
        
        $fileName = time().'.'.$file->getClientOriginalName();
        $path = 'upload/pets/';
        $file->move(public_path($path), $fileName);

        $request->merge(['picture' => $path.$fileName]);

        $request->request->add(['user_id' => Auth::user()->id]);

        return $request;
    }

    /**
     * Método Retorna um animal
     *
     * @param  int     $id  O identificador
     *
     * @return <type>  ( description_of_the_return_value )
     */
    public function findOrFailAnimal(int $id)
    {
        return $this->animalRepository->findOrFail($id);
    }

    /**
     * Método Edita um contato
     *
     * @param  array   $data  Os dados
     * @param  int     $id    O identificador
     *
     * @return <type>  ( description_of_the_return_value )
     */
    public function updateAnimal(array $data, int $id)
    {
        return $this->animalRepository->update($data, $id);
    }

    /**
     * Deleta um contato
     *
     * @param  int     $id  O identificador
     *
     * @return <type>  ( description_of_the_return_value )
     */
    public function destroyAnimal(int $id)
    {
        return $this->animalRepository->destroy($id);
    }

    /**
     * Stores a finder.
     */
    public function storeFinder(Request $request)
    {
        return $this->finderRepository->store($request->all());
    }
}